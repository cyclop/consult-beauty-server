"use strict";

const Hugla = require('hugla');

const Users = require('./models/users.js');

class App extends Hugla {
  constructor() {
    super(__dirname, __dirname + '/config.json');

    this.on('ready', function() {
      this.users = new Users(this);
    });
  }
}

module.exports = App;
