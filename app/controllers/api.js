"use strict";

const jwt = require('jsonwebtoken');
const cors = require('cors');

const app = global.app;

module.exports = function (router) {
  router.all('*', cors());

  router.post('/auth', function (req, res) {
    const email = req.body.email;
    const password = req.body.password;

    app.users.verify(email, password, function(err, user) {
      if (err) {
        res.status(500).json({
          status: 'fail',
          error: err.message
        });
        return;
      }

      if (!user) {
        res.json({
          status: 'fail',
          message: 'user not found'
        });
        return;
      }

      jwt.sign({ uid: user._id }, 'shhhhh', {
        iat: Math.floor(Date.now() / 1000),
        scopes: ["user"]
      }, function(token) {
        delete user.password;

        res.json({
          status: 'ok',
          token: token,
          user: user
        });
      });
    });
  });

  router.post('/register', function(req, res) {
    app.users.add(req.body.user, function(err, user) {
      if (err) {
        res.status(500).json({
          status: 'fail',
          error: err.message
        });
        return;
      }

      jwt.sign({ uid: user._id }, 'shhhhh', {
        iat: Math.floor(Date.now() / 1000),
        scopes: ["user"]
      }, function(token) {
        delete user.password;

        res.json({
          status: 'ok',
          token: token,
          user: user
        });
      });
    });
  });

  router.all('/app/*', function(req, res, next) {
    const authorizationHeader = req.headers.authorization;

    if (!authorizationHeader) {
      return res.status(401).send('no authorization header was found');
    }

    if (authorizationHeader.indexOf('Bearer') === -1) {
      return res.status(401).send('invalid authorization header');
    }

    const token = authorizationHeader.replace('Bearer', '').trim();

    jwt.verify(token, 'shhhhh', function(err, decoded) {
      if (err) {
        console.error(err);
        return res.status(401).send('error during authorization');
      }

      if (!decoded.uid) {
        return res.status(401).send('authorization failed');
      }

      const uid = decoded.uid;

      app.users.getById(uid, function(err, user) {
        if (err) {
          console.error(err);
          return res.status(401).send('error during authorization');
        }

        if (!user) {
          return res.status(401).send('authorization failed');
        }

        req.user = user;

        next();
      });
    });
  });

  router.post('/app/user/update', function(req, res) {
    const update = req.body.update;
    delete update['_id'];

    app.users.updateUser(req.user._id, update, function(err) {
      if (err) {
        console.error(err);
        return res.json({
          status: 'fail',
          message: 'update failed'
        });
      }

      res.json({
        status: 'ok'
      });
    });
  });

  router.post('/app/user/password/update', function(req, res) {
    const update = req.body.update;
    const oldPassword = update.old;
    const newPassword = update.new;
    const confirm = update.confirm;

    if (newPassword != confirm) {
      return res.json({
        status: 'error',
        message: 'password and confirmation don\'t match'
      });
    }

    app.users.verifyPassword(req.user._id, oldPassword, function(err, user) {
      if (err) {
        console.error(err);
        return res.json({
          status: 'fail',
          message: 'update failed'
        });
      }

      if (!user) {
        return res.json({
          status: 'error',
          message: 'old password is wrong'
        });
      }

      app.users.updatePassword(req.user._id, newPassword, function(err) {
        if (err) {
          console.error(err);
          return res.json({
            status: 'fail',
            message: 'update failed'
          });
        }

        res.json({
          status: 'ok',
          message: 'updated'
        });
      });
    });
  });

  router.post('/app/client/create', function() {

  });

  router.post('/app/client/update', function() {

  });

  router.post('/app/client/delete', function() {

  });


};
