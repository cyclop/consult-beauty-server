"use strict";

const bcrypt = require('bcrypt');
const mongodb = require('mongodb');

class Users {
  constructor(app) {
    this.app = app;

    this.app.mongodb.collection("users", function(err, users) {
      users.createIndexes([{ key: { "email": 1 }, name: "email", unique: true }], function(err) {
        if (err) {
          console.log(err);
        }
      });
    });
  }

  add(user, done) {
    bcrypt.hash(user.password, 8, function(err, hash) {
      if (err) {
        done(err);
        return;
      }

      user.password = hash;

      this.app.mongodb.collection("users", function(err, users) {
        if (err) {
          done(err);
          return;
        }

        user.registerDate = new Date();

        users.insertOne(user, {w:1}, function(err, r) {
          if (err) {
            done(err);
            return;
          }

          done(null, r.ops[0]);
        });
      });
    }.bind(this));
  }

  verify(email, password, done) {
    this.app.mongodb.collection("users", function(err, users) {
      if (err) {
        done(err);
        return;
      }

      users.findOne({ email: email }, function(err, user) {
        if (err) {
          done(err);
          return;
        }

        if (!user) {
          done();
          return;
        }

        bcrypt.compare(password, user.password, function(err, equal) {
          if (err) {
            done(err);
            return;
          }

          done(null, equal ? user : undefined);
        });
      });
    });
  }

  verifyPassword(id, password, done) {
    this.app.mongodb.collection("users", function(err, users) {
      if (err) {
        done(err);
        return;
      }

      users.findOne({ _id: mongodb.ObjectID(id) }, function(err, user) {
        if (err) {
          done(err);
          return;
        }

        if (!user) {
          done();
          return;
        }

        bcrypt.compare(password, user.password, function(err, equal) {
          if (err) {
            done(err);
            return;
          }

          done(null, equal ? user : undefined);
        });
      });
    });
  }

  updatePassword(id, password, done) {
    bcrypt.hash(password, 8, function(err, hash) {
      if (err) {
        done(err);
        return;
      }

      this.app.mongodb.collection("users", function(err, users) {
        if (err) {
          done(err);
          return;
        }

        users.updateOne({ _id: mongodb.ObjectID(id) }, { $set: { password: hash } }, { w: 1 }, function(err, result) {
          if (err) {
            done(err);
            return;
          }

          done();
        });
      });
    }.bind(this));
  }

  getById(id, done) {
    this.app.mongodb.collection("users", function(err, users) {
      if (err) {
        done(err);
        return;
      }

      users.findOne({ _id: mongodb.ObjectID(id) }, function(err, user) {
        if (err) {
          done(err);
          return;
        }

        if (!user) {
          done();
          return;
        }

        done(null, user);
      });
    });
  }

  updateUser(id, update, done) {
    this.app.mongodb.collection("users", function(err, users) {
      if (err) {
        done(err);
        return;
      }

      users.updateOne({ _id: mongodb.ObjectID(id) }, { $set: update }, { w: 1 }, function(err, result) {
        if (err) {
          done(err);
          return;
        }

        done();
      });
    });
  }
}

module.exports = Users;
