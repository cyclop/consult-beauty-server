"use strict";

var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpCopy = require('gulp-copy');
var del = require('del');

gulp.task('clean:assets', function(cb) {
    del([
        'assets'
    ], cb);
});

gulp.task('build', function() {
    gulp.src('./app/assets/fonts/**/*.*')
        .pipe(gulpCopy('./assets/fonts', { prefix: 3 }));

    gulp.src('./app/assets/images/**/*.*')
        .pipe(gulpCopy('./assets/images', { prefix: 3 }));

    gulp.src('./app/assets/scripts/**/*.*')
        .pipe(gulpCopy('./assets/scripts', { prefix: 3 }));

    return gulp.src('./app/assets/styles/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/styles'));
});

gulp.task('default', ['clean:assets', 'build'], function() {
    // place code for your default task here
});