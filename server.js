"use strict";

const app = global.app = new (require('./app/app.js'));

app.on("ready", function() {
  app.run();
});
